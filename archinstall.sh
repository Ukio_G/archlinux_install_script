#!/bin/bash

#######################
###### VARIABLES ######
#######################

## WiFi Password\Login ##
# If not used - set WIFI_USE to false
WIFI_USE=false
WIFI_LOGIN=OksanaR
WIFI_PASSWORD=572569845142


## Config Paths ##
LOCALE_CONF="/etc/locale.conf"
LOCALE_GEN="/etc/locale.gen"
WPA_SUPPLICANT_CONF="/etc/wpa_supplicant/wpa_supplicant.conf"
EFI_DIR="/sys/firmware/efi/efivars"


## Debugging and logging ##
LOG_FILE="install.log"              # Куда записывать лог
ERROR_LOG_FILE="error_install.log"  # Куда записывать лог
WRITE_TO_LOG=true                   # Будем ли мы вести логи или нет
ADDITIONAL_INFO=true                # Расширенное ведение логов
WRITE_TO_END=false                  # Будет или нет удален лог предыдущего запуска, если нет, то дописываем в конец файла, выделяя начало нового файла


### Settings ###
## Keyboard, locale, encoding, etc...
KEYS_TO_LOAD=ru                           # loadkeys $KEYS_TO_LOAD
CONSOLE_FONT=UniCyr_8x16                  # setfont $CONSOLE_FONT
LOCALE_CONF_CONTENT="LANG=ru_RU.UTF-8"    # echo $LOCALE_CONF_CONTENT > $LOCALE_CONF
LOCALE_GEN_CONTENT="ru_RU.UTF-8 UTF-8"    # echo $LOCALE_GEN_CONTENT >> $LOCALE_GEN


## Partitioning & boot
UEFI_INSTALL=false
ERARSE_DISK=true            # Удалить все разделы и вообще очистить рабочий диск перед тем, как начать разметку

if $UEFI_INSTALL; then
	ALIGNMENT_SECTOR=2047       # Конечный сектор, после которого начинается разметка. 2048 секторов - 1 МБ
else
	ALIGNMENT_SECTOR=31
fi


## Definition partition list
# Define one-dimentional array, which will contain all partition's mount points in correct order
if $UEFI_INSTALL; then
    list_mount_point=(\/boot \/ \/opt \/var swap \/home)
    list_size_mount_points=(512 1024*5 1024*5 1024*5 1024*2 1024*5)
else
    list_mount_point=(\/ \/opt \/var swap \/home)
    list_size_mount_points=(1024*5 1024*5 1024*5 1024*2 1024*5)
fi


# Declare properties, like array, where array name - property, mount point partition - key,
# result value - value of this proper nty for this partition with this mount point
declare -A size_mib         # size in MiB
declare -A hexcode_type     # partition type
declare -A sdX_num          # n in /dev/sdXn
declare -A max_size_sub     # possible size witch available to substract, if there is not enough space on used device.
numberParts=0


for mountPoint in ${!list_mount_point[*]}
  do

    if [[ $mountPoint==0 ]]; then
      DISC_ITEMS_DIALOG="$DISC_ITEMS_DIALOG $mountPoint ${DISCS[$mountPoint]} on"
    else
      DISC_ITEMS_DIALOG="$DISC_ITEMS_DIALOG $mountPoint ${DISCS[$mountPoint]} off"
    fi
  done >&7



if $UEFI_INSTALL; then
# /boot
size_mib[/boot]=512         # Размер раздела в МБ
hexcode_type[/boot]=EF00    # Хекскод типа файловой системы (Тип ФС+)
sdX_num[/boot]=numberParts  # Номер устройства - т.е. например 1 => /dev/sda1 ; 2 => /dev/sda2 ...
max_size_sub[/boot]=0       # Пока не используется. Используется как максимальное значаение, на которое раздел может быть уменьшен, если сумма заданных размеров
			    # всех разделов больше, чем места на устройстве
numberParts=$numberParts+1
fi

# / (root)
size_mib[/]=$((1024*2))
hexcode_type[/]=8300
sdX_num[/]=numberParts
max_size_sub[/]=0
numberParts=$numberParts+1

# /opt
size_mib[/opt]=$((1024*1))
hexcode_type[/opt]=8300
sdX_num[/opt]=numberParts
max_size_sub[/opt]=0
numberParts=$numberParts+1

# /var
size_mib[/var]=$((1024*1))
hexcode_type[/var]=8300
sdX_num[/var]=numberParts
max_size_sub[/var]=0
numberParts=$numberParts+1

# swap
size_mib[swap]=$((1024*1))
hexcode_type[swap]=8200
sdX_num[swap]=numberParts
max_size_sub[swap]=2048
numberParts=$numberParts+1

# /home
size_mib[/home]=$((1024*1))
hexcode_type[/home]=8300
sdX_num[/home]=numberParts
max_size_sub[/home]=$((1024*1))
numberParts=$numberParts+1


####################################################
################## Logging #########################
################## Cleanup #########################
####################################################


## Functions list ##
# Очищаем значения переменных в скрипте после предыдущих запусков
function clean_variables()
{
  sector_size=""
  DISCS=""
  select_disk=""
  selected_disk_index=""
  DISC_ITEMS_DIALOG=""
  WIRE_IFACE=""
  WLAN_IFACE=""
}

function apply_log_settings()
{
  if $WRITE_TO_LOG; then
    if [ $WRITE_TO_END = false ]; then
      rm "$PWD/$LOG_FILE"
      rm "$PWD/$ERROR_LOG_FILE"
    fi
    exec 3>&1
    exec 4>&2
    exec 6>"$PWD/$LOG_FILE"
    exec 7>"$PWD/$ERROR_LOG_FILE"
    exec 1>&6
    exec 2>&7

    if $ADDITIONAL_INFO; then
      exec 8>"$PWD/$LOG_FILE"
    else
      exec 8>/dev/null
    fi
  fi
}






####################################################
################## Basic   #########################
################## Config  #########################
####################################################







# Локаль, шрифт и прочее
function locale_config()
{
  setfont $CONSOLE_FONT
  loadkeys $KEYS_TO_LOAD
  echo $LOCALE_CONF_CONTENT > $LOCALE_CONF
  if [ -z `cat $LOCALE_GEN | grep -E -o '^$LOCALE_CONF_CONTENT$'` ]; then
    echo $LOCALE_GEN_CONTENT >> $LOCALE_GEN
  else
    echo -e "$LOCALE_GEN file has already configured. Nothing to change in it.\n\n"
  fi
  locale-gen
}

# Проверяем, корректно мы загрузились под UEFI или нет
function uefi_check()
{
  if [ ! -d "$EFI_DIR" ]; then
    echo -e "EE: Error! No UEFI catalog exist. System was loaded like BIOS-mode. Exit.\n\n" >&2
    exit
  fi

  echo -e "UEFI Ok\n\n"
}

# Поднимаем сеть
function network_config()
{
  if $WIFI_USE; then
    if [ ! "`iw dev`" ]; then
      echo -e "EE: No avaliable wireless interface specified. Change WIFI_USE flag to false, or check wi-fi device connect\n" >&2
      exit
    else
      WLAN_IFACE=`iw dev | grep -B -o 'wl.*'`
      echo -e "connecting to WiFi network $WIFI_LOGIN via $WLAN_IFACE interface\n"
      wpa_passphrase $WIFI_LOGIN $WIFI_PASSOWRD > $WPA_SUPPLICANT_CONF
      wpa_supplicant -c $WPA_SUPPLICANT_CONF -i $WLAN_IFACE -B
      echo -e "Connected to $WIFI_LOGIN" >&3 >&1
    fi
  else
    WIRE_IFACE=`ifconfig | grep -E -o 'enp[0-9a-z]*'`
    echo -e "get IP via dhclient to WIRE interface $WIRE_IFACE\n\n"
    dhclient $WIRE_IFACE
  fi
}





####################################################
################## Disk select #####################
##################    menu     #####################
####################################################







# Выбор диска для последующих операций разметки и форматирования
function select_disk()
{
  DISCS=("`fdisk -l | grep -E -w -o "/dev/sd[a-z]"`")

  echo -e "test DISCS value - $DISCS\n\n" >&8

  for disc in ${!DISCS[*]}
  do
    if [[ $disc==0 ]]; then
      DISC_ITEMS_DIALOG="$DISC_ITEMS_DIALOG $disc ${DISCS[$disc]} on"
    else
      DISC_ITEMS_DIALOG="$DISC_ITEMS_DIALOG $disc ${DISCS[$disc]} off"
    fi
  done >&7

  echo -e "test DISC_ITEMS_DIALOG value - $DISC_ITEMS_DIALOG \n\n" >&8
  exec 1>&3
  exec 2>&4
  exec 3>&-
  exec 4>&-
  exec 3>&-
  exec 7>&-
  exec 8>&-
  selected_disk_index=`dialog --radiolist "Select disk, where will system install" 20 30 40 $DISC_ITEMS_DIALOG 3>&1 1>&2 2>&6`
  selected_disk=${DISCS[$selected_disk_index]}
  apply_log_settings
  echo -e "selected disk $selected_disk.\n\n"
}





####################################################
##################      Disk     ###################
##################  calculation  ###################
####################################################



# Вытаскиваем из информации о диске размер одного кластера в байтах
function sector_size()
{
  sector_size=(`sgdisk -p $selected_disk | awk '/[Ss]ector size/' | grep -oE '[0-9]*'`)

  if [[ ${#sector_size[@]} > 1 ]]; then
    sector_size=${sector_size[1]}
  else
    sector_size=${sector_size[0]}
  fi

  echo -e "sector size is $sector_size\n"
}



#   1 сектор = 512 байт
#   1 КБайт  = 1024 байт = 512 байт + 512 байт = 1 сектор + 1 сектор = 2 сектора
#   1 МБайт  = 1024 КБайт = 512 КБайт + 512 КБайт = 1024 секторов + 1024 секторов = 2048 секторов
#   1 ГБайт  = 1024 МБайт = 512 МБайт + 512 МБайт = 524288 секторов + 524288 секторов = 1048576 секторов
function is_disk_space_enough() # Тут мы будем определять, влезает наша разметка без уменьшения разделов, или нет
{
  partitions_count=${#list_mount_point[@]} # Количество разделов
  partitions_mib_size=0                    # Занятое место всеми разделами, в МиБ

  available_sectors=$((`sgdisk -E $selected_disk`-`sgdisk -F $selected_disk`)) # Цельный кусок свободных секторов, с которым мы и будем работать
  last_available_sector=`sgdisk -E $selected_disk` # Какой крайний не занятый сектор?
  unaligned_sectors=$(($last_available_sector%2048)) # Возможно нам придется сместить начало сектора в одну из сторон. Тут храниться кол-во секторов, которые находятся дальше кратного сектора
  last_aligned_sector=$(($last_aligned_sector-($unaligned_sectors+1))) # И если такая необходимость ворзникнет - тут уже все посчитано, просто подставить надо

  local partition_start_sector=$(($ALIGNMENT_SECTOR+1))

  echo "Calculate aligned sectors positions for future disk partitions" >&8
  for i in ${!list_mount_point[*]}
  do
    local partition_name=${list_mount_point[$i]}
    local lenght_sector=$((${size_mib[$partition_name]}*2048)) # Длинна раздела в секторах
    local partition_last_sector=$(($ALIGNMENT_SECTOR+$lenght_sector))
    {
      echo -e "############"
      echo -e "####  About $partition_name "
      echo -e "############"
      echo -e "#    partition_start_sector    = $partition_start_sector \tmodtest result = $(($partition_start_sector%2048))"
      echo -e "#    partition_last_sector     = $partition_last_sector  \tmodtest result = $((($partition_last_sector+1)%2048))"
      echo -e "#    lenght in sectors         = $lenght_sector"
      echo -e "############\n"
    } >&8


    partition_start_sector=$(($partition_last_sector+1))
    partition_lenght_sectors=$(($partition_lenght_sectors+$lenght_sector))
  done

  {
    echo -e "############"
    echo -e "####  About $selected_disk "
    echo -e "############"
    echo -e "#    available_sectors (non GPT,first and last 2048 sectors, etc) - $available_sectors in $selected_disk"
    echo -e "#    last modificable sector index - `sgdisk -E $selected_disk`"
    echo -e "#    sectors used by current partitions config - $partition_lenght_sectors"
  }>&8

  if (( $available_sectors>$partition_lenght_sectors )); then
    echo -e "#    on the disk enough available space to use this partitioning configuration"
    echo -e "############\n"
    return 0
  else
    echo -e "#    on the disk isn't enough available space to use this partitioning configuration"
    echo -e "############\n"
    exit 16
  fi >&8
}



####################################################
##################      Disk     ###################
##################  manipulation ###################
####################################################





function partitioning()
{
  last_available_sector=`sgdisk -E $selected_disk | grep -oE "[0-9]*"` # Какой крайний не занятый сектор?
  unaligned_sectors=$(($last_available_sector%2048)) # Возможно нам придется сместить начало сектора в одну из сторон. Тут храниться кол-во секторов, которые находятся дальше кратного сектора
  last_aligned_sector=$(($last_aligned_sector-($unaligned_sectors+1))) # И если такая необходимость ворзникнет - тут уже все посчитано, просто подставить надо

  local partition_start_sector=$(($ALIGNMENT_SECTOR+1))
  partition_lenght_sectors=0
  for i in ${!list_mount_point[*]}
  do
    local partition_name=${list_mount_point[$i]}
    local lenght_sector=$((${size_mib[$partition_name]}*2048)) # Длинна раздела в секторах
    partition_lenght_sectors=$(($partition_lenght_sectors+$lenght_sector))
    local partition_last_sector=$(($ALIGNMENT_SECTOR+$partition_lenght_sectors))

    echo -e "Try to create partition $i ($partition_name), start - $partition_start_sector,  end - $partition_last_sector \n lenght $lenght_sector, partition_lenght_sectors $partition_lenght_sectors, size MiB ${size_mib[$partition_name]}" >&3 >&1

    createPartition $partition_start_sector $partition_last_sector $partition_name
    partprobe $selected_disk
    partition_start_sector=$(($partition_last_sector+1))
  done
}




function createPartition() # Аргументы - начало и конец раздела (номера кластеров, не КиБ/Кб/МиБ/МБ...)
{
  local p_type_hexcode=${hexcode_type[$3]}
  [ $3 = "swap" ] && p_type_hexcode=8300
  p_sdX_num=$selected_disk${sdX_num[$3]}
  partprobe $selected_disk
  echo "sgdisk -n 0:$1:$2 -t 0:$p_type_hexcode $selected_disk" >&8
  sgdisk -n 0:$1:$2 -t 0:$p_type_hexcode $selected_disk
  if [[ $? -eq 0 ]]; then
    echo -e "--------------//--------------" >&3 >&1
    echo -e "A new diskpart $partition_name created sucsessfully." >&3 >&1
    echo "Current partition, on which try create filesystem - $p_sdX_num"
    partprobe $selected_disk
    if [[ $3 == /boot ]]; then                  # Загрузочный раздел делаем как fat32
    echo -e "Try to create FAT32 filesystem"
    mkfs.vfat -F32 $p_sdX_num
    if [[ $? -eq 0 ]]; then
      echo -e "Filesystem has been set sucsessfully on $p_sdX_num\n" >&3 >&1
    else
      echo -e "EE: Filesystem hasn't been set sucsessfully on $p_sdX_num. Abort install.\n" >&1 >&2 >&3
      exit 21
    fi
    return;
    #  elif [[ $3 == swap ]]; then                 # Раздел подкачки тоже у нас далеко не ext4
    #  echo -e "Try to create swap partition"
    #  mkswap $p_sdX_num
  else                                        # Остальные разделы как правило просто ext4
    echo -e "Try to create EXT4 filesystem"
    mkfs.ext4 $p_sdX_num
  fi
  if [[ $? -eq 0 ]]; then
    echo -e "Filesystem has been set sucsessfully on $p_sdX_num\n" >&3 >&1
    partprobe $selected_disk
    return 0
  else
    echo "EE: Filesystem hasn't been set on $p_sdX_num. Install will aborted." >&2 >&1
    exit 17
  fi
else
  echo -e "EE: Creating new diskpart $3 ($p_sdX_num)  was failed.\n" >&3 >&2
  exit 17
fi
}



function clean_hard_disk {
  partprobe $selected_disk
  umount_all
  partprobe $selected_disk
  sgdisk $selected_disk -Z
  partprobe $selected_disk
}





####################################################
##################      Disk     ###################
##################    [u]mount   ###################
####################################################




function umount_all {
  # Сначала отключаем все, что не /, например swap
  partprobe $selected_disk

  if [[ `cat /proc/swaps | grep $selected_disk${sdX_num[swap]}` ]]; then
    echo "/proc/swaps contain $selected_disk${sdX_num[swap]}. Try to swapoff it." >&8
    # swapoff $selected_disk${sdX_num[swap]} >&8 2>&8
    partprobe $selected_disk >&8
    [ $? -eq 0 ] || echo "swapoff $selected_disk${sdX_num[swap]} fail."
    if [[ ! `cat /proc/swaps | grep $selected_disk${sdX_num[swap]}` ]]; then
      echo "/proc/swaps clean. $selected_disk${sdX_num[swap]} is not mounted now." >&8
    else
      echo "EE: For reasons unknow, /proc/swaps contain $selected_disk${sdX_num[swap]} after \`swapoff $selected_disk${sdX_num[swap]}\`. Exit from install." >&8 >&2
      exit 20
    fi
  fi

  findmnt /mnt >&8

  if [[ `findmnt /mnt` ]]; then
    echo "findmnt /mnt found mounted partitions. Try recursive umount it and subparts inside." >&8
    umount -R /mnt
    [ $? -eq 0 ] || echo "EE: umount -R /mnt fail." >&2 >&8
  fi

  findmnt /mnt >&8

  if [[ `findmnt /var/cache/pacman/pkg` ]]; then
    echo "findmnt /var/cache/pacman/pkg found mounted partitions. Try recursive umount it and subparts inside." >&8
    umount -R /var/cache/pacman/pkg
    [ $? -eq 0 ] || echo "EE: umount -R /var/cache/pacman/pkg fail." >&2 >&8
  fi


  partprobe $selected_disk
}

function mount_created_partition {
  mount $selected_disk${sdX_num[/]} /mnt
  [ $? -eq 0 ] || die "EE: mount $selected_disk${sdX_num[/]} to /mnt failed. Abort install." >&3 >&2
  for i in ${!list_mount_point[*]}
  do
    pName=${list_mount_point[$i]}
    echo "Try mount $selected_disk${sdX_num[$pName]}($pName) to /mnt$pName">&8
    if [[ ! $pName == / ]] && [[ ! $pName == swap ]]; then
      [ -e /mnt$pName ] || mkdir /mnt$pName
      if [ $? -eq 0 ]; then
        echo "mount \"$selected_disk${sdX_num[$pName]}\" \"/mnt$pName\"" >&8
        mount "$selected_disk${sdX_num[$pName]}" /mnt$pName
      else
        echo -e "EE: something trouble with mount $selected_disk${sdX_num[$pName]} to /mnt$pName. Exit" >&2
        exit 18
      fi
    fi
  done

  #swapon "$selected_disk${sdX_num[swap]}"
  if [[ ! $? -eq 0 ]]; then
    echo -e "EE: something trouble with turn swap on. Exit" >&2
    exit 19
  fi
}






####################################################
##################      Post      ##################
##################     Install    ##################
####################################################



function prepeare_new_system {


  if $DOWNLOAD_PACKAGE; then
    pacstrap -c /mnt base >&3
  fi


   if [[ $? -eq 0 ]]; then
    genfstab -U /mnt >> /mnt/etc/fstab
    cp /etc/resolv.conf /mnt/etc/resolv.conf
    cp ./archinstall_post.sh /mnt/root/
    arch-chroot /mnt
  else
    echo "EE: pacstrap work failed. Abort" >&2
    exit 22
  fi

}

# Установка начинается отсюда

apply_log_settings
clean_variables
uefi_check
select_disk
partprobe $selected_disk
sector_size
is_disk_space_enough
if $ERARSE_DISK; then
  echo "ERARSE_DISK is true - try to erarse disk table and partitions"
  clean_hard_disk
fi
partprobe $selected_disk
partitioning
partprobe $selected_disk
mount_created_partition
partprobe $selected_disk
prepeare_new_system
